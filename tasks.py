import os

from invoke import task


@task
def mk(c, path="pony.img", size="1G", dev_name="pony", mnt_point="/media/pony", cipher="aes-xts-plain64", mkfs="mkfs.xfs"):
    "make the encrypted sparse drive"
    c.run(f"truncate -s {size} {path}")
    c.run(f"sudo losetup --find {path}")
    loop = c.run(f"sudo losetup -j {path}").stdout.split(":")[0]
    c.run(f"sudo cryptsetup -v --cipher {cipher} --key-size 512 --hash sha384 --iter-time 5000 --use-random --verify-passphrase --integrity hmac-sha256 --pbkdf argon2i luksFormat --type luks2 {loop}", pty=True)
    c.run(f"sudo cryptsetup luksOpen {loop} {dev_name}", pty=True)
    c.run(f"sudo {mkfs} /dev/mapper/{dev_name}")
    # Load it
    check_mount_point(c, mnt_point)
    c.run(f"sudo mount /dev/mapper/{dev_name} {mnt_point}")
    c.run(f"sudo chown {os.getenv('USER')} {mnt_point}")
    with c.cd(mnt_point):
        no_history_shell(c)


@task
def mount(c, dev_name="pony", path="pony.img", mnt_point="/media/pony"):
    "mount your drive and drop into a no-history shell in it"
    c.run("sudo losetup -j {path}").stdout
    c.run(f"sudo losetup --find {path}")
    loop = c.run(f"sudo losetup -j {path}").stdout.split(":")[0]
    c.run(f"sudo cryptsetup luksOpen {loop} {dev_name}", pty=True)
    check_mount_point(c, mnt_point)
    c.run(f"sudo mount /dev/mapper/{dev_name} {mnt_point}")
    with c.cd(mnt_point):
        no_history_shell(c)


@task
def shutdown(c, dev_name="pony", mnt_point="/media/pony"):
    "unmount everything when you're done"
    c.run(f"sudo umount {mnt_point}", warn=True)
    c.run(f"sudo cryptsetup close /dev/mapper/{dev_name}", warn=True)
    c.run("sudo losetup -D", warn=True)


def get_cs_version(c):
    return c.run("cryptsetup --version").stdout.split()

@task
def supported_crypto(c):
    chunked = c.run("cat /proc/crypto", hide=True).stdout.split("/n/n")
    return [{line.split(":")[0].strip(): line.split(":")[1].strip() for line in chunk.split("\n")} for chunk in chunked[:-1]]


def check_losetup(c):
    c.run("sudo losetup -J")
    # lsblk /dev/loop7 -J


def check_mount_point(c, path) -> None:
    "ensure path exists"
    if not os.path.exists(path):
        c.run(f"sudo mkdir -p {path}")


def no_history_shell(c) -> None:
    "spawn a no-history shell"
    if os.getenv("TMUX"):
        c.run("tmux set-option -w allow-rename off")
        c.run("tmux rename-window 'ponycrypt'")
    c.run(os.getenv("SHELL"), env={"HISTFILE": "/dev/null"}, pty=True)
    if os.getenv("TMUX"):
        c.run("tmux set-window-option automatic-rename 'on'")
        c.run("tmux set-option -w allow-rename on")
